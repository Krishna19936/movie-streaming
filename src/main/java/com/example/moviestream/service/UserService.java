package com.example.moviestream.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.example.moviestream.entity.Movie;
import com.example.moviestream.entity.User;
import com.example.moviestream.exception.EmailorPasswordException;

import com.example.moviestream.exception.MovielistNotFoundException;

import com.example.moviestream.repository.MovieRepository;
import com.example.moviestream.repository.UserRepository;



@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	
    @Autowired
    private MovieRepository movieRepository;
	
	public User registerUser(User user) {
		
		
		String emailvalid = user.getUserEmail();
		String acctype = user.getUserAccountType();
		Optional<User> validuser = userRepository.checkuserexits(emailvalid);
		if((acctype.equalsIgnoreCase("paid") || acctype.equalsIgnoreCase("trial")) && !validuser.isPresent()) {
		
		userRepository.save(user);
		return user;
		}
		
		else {
			
			return null;
		}
	}
	
	public User login(User user) throws EmailorPasswordException {
        User user1 = userRepository.findByuserEmailAnduserPassword(user.getUserEmail(), user.getUserPassword());

 

        Optional<User> utlist = userRepository.fetchUserwatchstatus(user.getUserEmail());

 

        if (user1 != null) {

 
        	
            if (!utlist.isPresent() && user1.getUserAccountType().equalsIgnoreCase("Trial")) {
            	
                throw new EmailorPasswordException();
            }
            
            user1.setUserPassword("");
            
            return user1;
        }

 

        return null;

 

    }
	
	public List<Movie> fetchMovielist(String name) throws MovielistNotFoundException {

		 

        List<Movie> list;
        if (name != null) {
            list = movieRepository.getyGenre(name);
            for (Movie m : list) {
                if (m.getGenre().equalsIgnoreCase(name) && m.getGenre() != null) {
                    return list;
                }

 

            }
        }
        list = movieRepository.fetchmovieByName(name);
        if (list.isEmpty()) {
            throw new MovielistNotFoundException();
        }
        return list;

 

    }

}
