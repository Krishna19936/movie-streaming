package com.example.moviestream.service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.moviestream.entity.Movie;
import com.example.moviestream.entity.User;
import com.example.moviestream.entity.UserMovie;
import com.example.moviestream.repository.MovieRepository;
import com.example.moviestream.repository.UserMovieRepository;
import com.example.moviestream.repository.UserRepository;




@Service
public class MovieService {
	
	@Autowired
	private MovieRepository movieRepository;
	
	@Autowired
	private UserMovieRepository usermovieRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	public List<Movie> fetchAllMovies(){
		
		return movieRepository.getallmovies();
		
		
	}
	
	
	public Movie watchMovie(UserMovie umovie) {
		
		Optional<Movie> mo = movieRepository.findById(umovie.getMovieId());
		Optional<User> us = userRepository.findById(umovie.getUserId());
		Optional<UserMovie> umo = usermovieRepository.checkuserwatchmovie(umovie.getUserId(),umovie.getMovieId());
		if(mo.isPresent() && us.isPresent()) {
		
		Movie m1 = mo.get();
		if(!umo.isPresent()) {
		usermovieRepository.save(umovie);
		}
		return m1;
		}
		return null;
	}
	
	public List<Movie> fetchUserWatchedMovies(long userId){
		
		
		List<Movie> usermovlist = new ArrayList<>();
		Optional<User> us = userRepository.findById(userId);
		Optional<List<UserMovie>> umwlist = usermovieRepository.fetchwatchedmovielist(userId);
		if(!umwlist.isPresent() || !us.isPresent()) {
			
		return usermovlist;
		}
		List<UserMovie> umwl = umwlist.get();
		UserMovie um2;
		for(int i = 0; i<umwl.size();i++) {
			
			
			um2 = umwl.get(i);
			Optional<Movie> om = movieRepository.getusermovies(um2.getMovieId());
		    if(om.isPresent()) {
		    	Movie mo = om.get();
		    	usermovlist.add(mo);
		    }
		
		}
		
		Comparator<Movie> compareByMovieName = (Movie o1, Movie o2) ->
        o1.getMovieName().compareTo( o2.getMovieName() );
        Collections.sort(usermovlist, compareByMovieName);
		return usermovlist;
		
	}

}
