package com.example.moviestream.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class PasswordMasterExceptionHandler {
	
	@ExceptionHandler(value = EmailorPasswordException.class)
    public ResponseEntity<String> handleEmailorPasswordException() {
      
        return new ResponseEntity<>("Please Pay to Continue Streaming",  HttpStatus.NOT_FOUND);
 }
  @ExceptionHandler(value = MovielistNotFoundException.class)
    public ResponseEntity<String> movielistException() {
      
        return new ResponseEntity<>("Movie or Genre List not found",  HttpStatus.NOT_FOUND);
 }

}
