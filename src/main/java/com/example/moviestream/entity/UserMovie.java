package com.example.moviestream.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "usermovie")
public class UserMovie {
	
		
		    @Id
		    @GeneratedValue(strategy = GenerationType.IDENTITY)
		    @Column(name = "userMovieId")
		    private long userMovieId;
			
			@Column(name = "userId")
		    private long userId;
			
			@Column(name = "movieId")
		    private long movieId;
			
			

			public long getUserMovieId() {
				return userMovieId;
			}

			public void setUserMovieId(long userMovieId) {
				this.userMovieId = userMovieId;
			}

			public long getUserId() {
				return userId;
			}

			public void setUserId(long userId) {
				this.userId = userId;
			}

			public long getMovieId() {
				return movieId;
			}

			public void setMovieId(long movieId) {
				this.movieId = movieId;
			}

			

			public UserMovie(long userMovieId, long userId, long movieId, boolean watched) {
				super();
				this.userMovieId = userMovieId;
				this.userId = userId;
				this.movieId = movieId;
				
			}

			public UserMovie() {
				super();
				
			}
			
			

}
