package com.example.moviestream.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name = "userId")
	    private long userId;
		
		@Column(name = "userEmail")
	    private String userEmail;
		
		@Column(name = "userName")
	    private String userName;
		
		@Column(name = "userPassword")
	    private String userPassword;
		
		@Column(name = "userAccountType")
	    private String userAccountType;
		
		@Column(name="regTime")
		private LocalDateTime regTime = LocalDateTime.now();
		
		

		public User() {
			super();
			
		}

		public User(long userId, String userEmail, String userName, String userPassword, String userAccountType,
				LocalDateTime regTime) {
			super();
			this.userId = userId;
			this.userEmail = userEmail;
			this.userName = userName;
			this.userPassword = userPassword;
			this.userAccountType = userAccountType;
			this.regTime = regTime;
		}

		public long getUserId() {
			return userId;
		}

		public void setUserId(long userId) {
			this.userId = userId;
		}

		public String getUserEmail() {
			return userEmail;
		}

		public void setUserEmail(String userEmail) {
			this.userEmail = userEmail;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getUserPassword() {
			return userPassword;
		}

		public void setUserPassword(String userPassword) {
			this.userPassword = userPassword;
		}

		public String getUserAccountType() {
			return userAccountType;
		}

		public void setUserAccountType(String userAccountType) {
			this.userAccountType = userAccountType;
		}

		public LocalDateTime getRegTime() {
			return regTime;
		}

		public void setRegTime(LocalDateTime regTime) {
			this.regTime = regTime;
		}
		

}
