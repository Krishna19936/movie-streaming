package com.example.moviestream.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "movie")
public class Movie {
	
		
		    @Id
		    @GeneratedValue(strategy = GenerationType.IDENTITY)
		    @Column(name = "movieId")
		    private long movieId;
			
			@Column(name = "movieName")
		    private String movieName;
			
			@Column(name = "moviedesc")
		    private String moviedesc;
			
			@Column(name = "genre")
		    private String genre;

			public long getMovieId() {
				return movieId;
			}

			public void setMovieId(long movieId) {
				this.movieId = movieId;
			}

			public String getMovieName() {
				return movieName;
			}

			public void setMovieName(String movieName) {
				this.movieName = movieName;
			}

			public String getMoviedesc() {
				return moviedesc;
			}

			public void setMoviedesc(String moviedesc) {
				this.moviedesc = moviedesc;
			}

			public String getGenre() {
				return genre;
			}

			public void setGenre(String genre) {
				this.genre = genre;
			}

			public Movie(long movieId, String movieName, String moviedesc, String genre) {
				super();
				this.movieId = movieId;
				this.movieName = movieName;
				this.moviedesc = moviedesc;
				this.genre = genre;
			}

			public Movie() {
				super();
				
			}
			
			

}
