package com.example.moviestream.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.moviestream.entity.Movie;
import com.example.moviestream.entity.User;
import com.example.moviestream.entity.UserMovie;
import com.example.moviestream.exception.EmailorPasswordException;
import com.example.moviestream.exception.ErrorMessage;
import com.example.moviestream.exception.MovielistNotFoundException;

import com.example.moviestream.service.MovieService;
import com.example.moviestream.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@EnableFeignClients
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/")
@Api(value = "MovieStreamingController")


public class MovieStreamingController {
	
	@Autowired
    private UserService userservice;
	
	@Autowired
    private MovieService movieservice;
	
	@PostMapping(path = "/users")
	@ApiOperation("Register a user")
	private ResponseEntity<User> userRegistration(
			@RequestBody User user) {
		
		
		User usr = userservice.registerUser(user);
		if (usr == null) {
			ErrorMessage error = new ErrorMessage();
            error.setErrormessage("User is already Registered");
            error.setErrorcode(400);
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
			
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(usr);
		
		
	}
	
	 /**
     * User login
     * 
     * @param userPassword
     * @param userEmail
     * @return message
     * @throws EmailorPasswordException
     */
    @ApiOperation("Login to application")
    @PostMapping(value = "/login", produces = "application/json")
    public ResponseEntity<User> login(@RequestBody User user) throws EmailorPasswordException {

 

        User user1 = userservice.login(user);
        
        
        if (user1 == null) {
			ErrorMessage error = new ErrorMessage();
            error.setErrormessage("Incorrect Email ID or Passowrd");
            error.setErrorcode(400);
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
			
		}
        else {
        
		return ResponseEntity.status(HttpStatus.CREATED).body(user1);
        }
    }
	
    
    
	    @ApiOperation("List all movies")
	    @GetMapping(path = "/movies")
	    public ResponseEntity<List<Movie>> listAllMovies() {

	    	if(movieservice.fetchAllMovies() == null) {
	    		ErrorMessage error = new ErrorMessage();
	            error.setErrormessage("No movie exits");
	            error.setErrorcode(404);
	            return new ResponseEntity(error, HttpStatus.NOT_FOUND);
	    		
	    	}
	        
	        return ResponseEntity.status(HttpStatus.OK).body(movieservice.fetchAllMovies());
	    }
	    
	    
	    @PostMapping(path = "/usermovies")
		@ApiOperation("user watch movie")
		private ResponseEntity<Movie> userWatchMovie(@RequestBody UserMovie umovie) {
			
			
			Movie mov = movieservice.watchMovie(umovie);
			if (mov == null) {
				ErrorMessage error = new ErrorMessage();
	            error.setErrormessage("Invalid inputs");
	            error.setErrorcode(400);
	            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
				
			}
			return ResponseEntity.status(HttpStatus.CREATED).body(mov);
			
			
		}
	    
	    @ApiOperation("List all movies user watched")
	    @GetMapping(path = "/users/{userId}/movies")
	    public ResponseEntity<List<Movie>> getUserWatchedMovies(@PathVariable("userId") long userId) {

	    	if(movieservice.fetchUserWatchedMovies(userId).isEmpty()) {
	    		
	    		ErrorMessage error = new ErrorMessage();
	            error.setErrormessage("User still not watched any movie");
	            error.setErrorcode(404);
	            return new ResponseEntity(error, HttpStatus.NOT_FOUND);
	    		
	    	}
	        
	        return ResponseEntity.status(HttpStatus.OK).body(movieservice.fetchUserWatchedMovies(userId));
	    }
	    
	    /**
	     * fetch Movie
	     *
	     * @param movieName
	     * @return movie
	     *
	     * @throws EmailorPasswordException
	     */
	    @ApiOperation("search  movies based on genre or movie name")
	    @GetMapping(value = "/movies/{name}")
	    public ResponseEntity<List<Movie>> fetchMoviesName(@RequestParam("name") String name)
	            throws MovielistNotFoundException {
	        
	        return new ResponseEntity<List<Movie>>(userservice.fetchMovielist(name), HttpStatus.OK);
	    }
	    
	    
	    

}
