package com.example.moviestream.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.example.moviestream.entity.UserMovie;

public interface UserMovieRepository extends JpaRepository<UserMovie,Long> {
	
	@Query(value="SELECT * FROM usermovie where user_id = ?1 and movie_id = ?2",nativeQuery=true)
	Optional<UserMovie> checkuserwatchmovie(long userId,long movieId);
	
	@Query(value="SELECT * FROM usermovie where user_id = ?1",nativeQuery=true)
	Optional<List<UserMovie>> fetchwatchedmovielist(long userId);
	

}
