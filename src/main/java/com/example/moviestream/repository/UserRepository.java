package com.example.moviestream.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.moviestream.entity.User;




public interface UserRepository extends JpaRepository<User,Long> {
	
	@Query(value="SELECT * FROM user where user_email = ?1",nativeQuery=true)
	Optional<User> checkuserexits(String useremail);
	
	@Query(value="SELECT * FROM user WHERE user_email = ?1 and user_password = ?2", nativeQuery = true)    
    User findByuserEmailAnduserPassword(String userEmail, String userPassword);

 

    @Query(value="SELECT * FROM user WHERE user_email = ?1 and reg_time >= NOW() - INTERVAL 1 DAY",nativeQuery=true)    
    Optional<User> fetchUserwatchstatus(String userEmail);
	

}
