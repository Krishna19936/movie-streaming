package com.example.moviestream.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.moviestream.entity.Movie;


@Repository
public interface MovieRepository extends JpaRepository<Movie,Long> {
	
	@Query(value="SELECT * FROM movie order by movie_name;",nativeQuery=true)
	List<Movie> getallmovies();
	
	@Query(value="SELECT * FROM movie where movie_id = ?1 ;",nativeQuery=true)
	Optional<Movie> getusermovies(long movieId);
	
	
	@Query(value = "SELECT * from movie m where m.movie_name like %:name ORDER BY m.movie_name DESC", nativeQuery = true)
    List<Movie> fetchmovieByName(String name);

 

    @Query(value = "SELECT * from movie m where m.genre=?1 ", nativeQuery = true)
    List<Movie> getyGenre(String name);

}
