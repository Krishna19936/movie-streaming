package com.example.moviestream.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.moviestream.entity.Movie;
import com.example.moviestream.entity.User;
import com.example.moviestream.exception.EmailorPasswordException;
import com.example.moviestream.exception.MovielistNotFoundException;
import com.example.moviestream.repository.MovieRepository;
import com.example.moviestream.repository.UserRepository;

@SpringBootTest
public class UserServiceTest {

	@InjectMocks
	UserService userService;

	@Mock
	UserRepository userRepository;

	@Mock
	MovieRepository movieRepository;

	@BeforeEach
	public void init() {

	}

	@Test
	public void TestregisterUser() {

		User user = new User();
		user.setUserAccountType("paid");
		user.setUserEmail("test@test.com");
		user.setUserName("sri");
		user.setUserId(1);
		user.setUserPassword("password");

		Optional<User> empty = Optional.empty();

		when(userRepository.checkuserexits("test@test.com")).thenReturn(empty);

		User result = userService.registerUser(user);
		assertEquals(1, result.getUserId());
		assertEquals("paid", result.getUserAccountType());
		assertEquals("test@test.com", result.getUserEmail());
		assertEquals("sri", result.getUserName());
		assertEquals("password", result.getUserPassword());

	}

	@Test
	public void TestregisterUser1() {

		User user = new User();
		user.setUserAccountType("Trial");
		user.setUserEmail("test@test.com");
		user.setUserName("sri");
		user.setUserId(1);
		user.setUserPassword("password");

		when(userRepository.checkuserexits("test@test.com")).thenReturn(Optional.of(user));

		User result1 = userService.registerUser(user);
		assertEquals(null, result1);

	}

	@Test
	public void testLogin() throws EmailorPasswordException {
		String userEmail = "vib";
		String userPassword = "12345";
		String UserAccountType = "paid";
		User u = new User();

		u.setUserEmail(userEmail);
		u.setUserPassword(userPassword);
		u.setUserAccountType(UserAccountType);
		System.out.println(u.getUserEmail() + "" + u.getUserPassword());
		System.out.println(userService.login(u));

		System.out.println(u.getUserAccountType() + ".........." + u.getUserPassword());
		when(userRepository.findByuserEmailAnduserPassword(userEmail, userPassword)).thenReturn(u);
		when(userRepository.fetchUserwatchstatus(userEmail)).thenReturn(Optional.of(u));
		
		User u2 = userService.login(u);
		assertEquals("paid", u2.getUserAccountType());

		
		

		// assertEquals("200 OK
		// OK,com.example.moviestream.exception.SuccessMessage@30555d6,[]",
		// userService.login(userEmail, userPassword));

	}

	@Test
	public void testgetAllMovie() throws MovielistNotFoundException {
		Movie m = new Movie();
		String movieNmae = "comedy";
		m.setMovieName(movieNmae);
		List<Movie> l = new ArrayList<>();
		l.add(m);
		//when(movieRepository.getyGenre(movieNmae)
		when(movieRepository.fetchmovieByName(movieNmae)).thenReturn(l);
		List<Movie> m1 = userService.fetchMovielist(movieNmae);
		Movie result = m1.get(0);
		
		assertEquals(null, result.getGenre());
	}
	
	
	@Test
	public void testgetAllMovie1() throws MovielistNotFoundException {
		Movie m = new Movie();
		String movieNmae = "comedy";
		m.setMovieName(movieNmae);
		List<Movie> l = new ArrayList<>();
		l.add(m);
		when(movieRepository.getyGenre(movieNmae)).thenReturn(l);
		when(movieRepository.fetchmovieByName(movieNmae)).thenReturn(l);
		List<Movie> m1 = userService.fetchMovielist(movieNmae);
		Movie result = m1.get(0);
		
		assertEquals("comedy", result.getGenre());
	}

}
