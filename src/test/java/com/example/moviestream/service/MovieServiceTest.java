package com.example.moviestream.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.moviestream.entity.Movie;
import com.example.moviestream.entity.User;
import com.example.moviestream.entity.UserMovie;
import com.example.moviestream.repository.UserMovieRepository;
import com.example.moviestream.repository.UserRepository;
import com.example.moviestream.repository.MovieRepository;






@SpringBootTest
public class MovieServiceTest {
	
	@InjectMocks
	MovieService movieService;
	
	@Mock
	UserRepository userRepository;
	
	@Mock
	MovieRepository movieRepository;
	
	@Mock
	UserMovieRepository usermovieRepository;
	
	@Test
	public void TestfetchAllMovies() {
		
		Movie mov = new Movie();
		mov.setGenre("horro");
		mov.setMoviedesc("watching movie");
		mov.setMovieId(1);
		mov.setMovieName("conjuring");
		List<Movie> mlist = new ArrayList<>();
		mlist.add(mov);
		when(movieRepository.getallmovies()).thenReturn(mlist);
		
		List<Movie> result = movieService.fetchAllMovies();
		Movie result1 = result.get(0);
		assertEquals(1, result1.getMovieId());
		assertEquals("horro", result1.getGenre());
		assertEquals("conjuring", result1.getMovieName());
		
		
	}
	
	@Test
	public void TestwatchMovie() {
		
		long movid = 1;
		long usrid =1;
		Movie mov = new Movie();
		mov.setGenre("horro");
		mov.setMoviedesc("watching movie");
		mov.setMovieId(1);
		mov.setMovieName("conjuring");
		

		User user = new User();
		user.setUserAccountType("paid");
		user.setUserEmail("test@test.com");
		user.setUserName("sri");
		user.setUserId(1);
		user.setUserPassword("password");
		
		UserMovie um = new UserMovie();
		um.setMovieId(1);
		um.setUserId(1);
		um.setUserMovieId(1);
		
		when(movieRepository.findById(movid)).thenReturn(Optional.of(mov));
		when(userRepository.findById(usrid)).thenReturn(Optional.of(user));
		when(usermovieRepository.checkuserwatchmovie(usrid,movid)).thenReturn(Optional.of(um));
		
		
		Movie result = movieService.watchMovie(um);
		assertEquals(1, result.getMovieId());
		assertEquals("horro", result.getGenre());
		assertEquals("conjuring", result.getMovieName());
		
		
	}
	
	@Test
	public void TestfetchUserWatchedMovies() {
		
		long movid = 1;
		long usrid =1;
		User user = new User();
		user.setUserAccountType("paid");
		user.setUserEmail("test@test.com");
		user.setUserName("sri");
		user.setUserId(1);
		user.setUserPassword("password");
		
		Movie mov = new Movie();
		mov.setGenre("horro");
		mov.setMoviedesc("watching movie");
		mov.setMovieId(1);
		mov.setMovieName("conjuring");
		
		UserMovie um = new UserMovie();
		um.setMovieId(1);
		um.setUserId(1);
		um.setUserMovieId(1);
		List<UserMovie> umlist = new ArrayList<>();
		umlist.add(um);
		
		when(userRepository.findById(usrid)).thenReturn(Optional.of(user));
		when(usermovieRepository.fetchwatchedmovielist(usrid)).thenReturn(Optional.of(umlist));
		when(movieRepository.getusermovies(movid)).thenReturn(Optional.of(mov));
		
		List<Movie> result1 = movieService.fetchUserWatchedMovies(usrid);
		Movie result = result1.get(0);
		assertEquals(1, result.getMovieId());
		assertEquals("horro", result.getGenre());
		assertEquals("conjuring", result.getMovieName());
		
		
}
	
	
	@Test
	public void TestfetchUserWatchedMovies1() {
		
		Optional<List<UserMovie>> empty = Optional.empty();
		Optional<User> emptyuser = Optional.empty();
		Optional<Movie> emptymov = Optional.empty();
	    
		
		long movid = 1;
		long usrid =1;
		
		
	
		
		when(userRepository.findById(usrid)).thenReturn(emptyuser);
		when(usermovieRepository.fetchwatchedmovielist(usrid)).thenReturn(empty);
		when(movieRepository.getusermovies(movid)).thenReturn(emptymov);
		
		List<Movie> result1 = movieService.fetchUserWatchedMovies(usrid);
		
		assertEquals(0, result1.size());
		
	}	
	
	
	
	@Test
	public void TestwatchMovie1() {
		
		long movid = 1;
		long usrid =1;
		UserMovie um = new UserMovie();
		um.setMovieId(1);
		um.setUserId(1);
		um.setUserMovieId(1);
		
		Optional<UserMovie> empty = Optional.empty();
		Optional<User> emptyuser = Optional.empty();
		Optional<Movie> emptymov = Optional.empty();
	
		
		when(userRepository.findById(usrid)).thenReturn(emptyuser);
		when(usermovieRepository.checkuserwatchmovie(usrid,movid)).thenReturn(empty);
		when(movieRepository.findById(movid)).thenReturn(emptymov);
		
		
		Movie result = movieService.watchMovie(um);
		assertEquals(null, result);
		
		
	}
	

}
